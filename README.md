# How to run
### Docker container
If you have docker installed on your machine, you can use image with all dependencies installed
```
docker pull xomiachuna/work-env
# on mac or linux
docker run --rm -it -p 8888:8888 -v $(pwd):/work xomiachuna/work-env
# on Windows
docker run --rm -it -p 8888:8888 -v "%cd%":/work xomiachuna/work-evn
```
### Plain Jupyter
Launch jupyter notebook as always, but keep in mind that some libraries (`opencv`, `ntlk`, `graphviz`) may require additional installation