FROM continuumio/miniconda3

# core utils
RUN conda install -y \
    pandas \
    jupyter \
    scikit-learn

# visualization
RUN conda install -y \
    matplotlib

# opencv
RUN conda install -y opencv

# Graphviz
RUN apt-get update && apt-get install -y graphviz

RUN conda install -y \
    python-graphviz \
    graphviz

RUN conda install h5py


VOLUME /work
WORKDIR /work

RUN echo "jupyter notebook --no-browser --ip=0.0.0.0 --port=8888 --allow-root" > ../run_jupyter.sh

CMD sh ../run_jupyter.sh